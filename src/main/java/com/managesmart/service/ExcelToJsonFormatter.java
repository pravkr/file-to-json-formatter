package com.managesmart.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;

@Service
public class ExcelToJsonFormatter {

	static class UploadUtils {

		static Supplier<Stream<Row>> getRowStreamSupplier(Iterable<Row> rows) {
			return () -> getStream(rows);
		}

		static <T> Stream<T> getStream(Iterable<T> iterable) {
			return StreamSupport.stream(iterable.spliterator(), false);
		}
		
		static Stream<Cell> getStream(Row row, int length) {
			return IntStream.range(0, length)
				.mapToObj(i -> row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL));
//			return StreamSupport.stream(iterable.spliterator(), false);
		}

		static Supplier<Stream<Integer>> cellIteratorSupplier(int end) {
			return () -> numberStream(end);
		}

		static Stream<Integer> numberStream(int end) {
			return IntStream.range(0, end).boxed();
		}
		
		static boolean isRowEmpty(Row row) {
		    for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
		        Cell cell = row.getCell(c);
		        if (cell != null && cell.getCellType() != CellType.BLANK)
		            return false;
		    }
		    return true;
		}
	}

	public List<Map<String, String>> getJsonFormatOf(InputStream data) throws IOException {
		DataFormatter formatter = new DataFormatter();
		Workbook workbook = WorkbookFactory.create(data);
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		Sheet sheet = workbook.getSheetAt(0);
		Supplier<Stream<Row>> rowStreamSupplier = UploadUtils.getRowStreamSupplier(sheet);
		Row headerRow = rowStreamSupplier.get().findFirst().get();
		List<String> headerCells = UploadUtils.getStream(headerRow)
				.filter(cell -> !(cell == null || cell.getCellType() == CellType.BLANK))
				.map(Cell::getStringCellValue)
				.collect(Collectors.toList());
		int colCount = headerCells.size();
		System.out.println("colCount: " + colCount);
		List<Map<String, String>> content = rowStreamSupplier
				.get()
				.skip(1)
				.filter(row -> !UploadUtils.isRowEmpty(row))
				.map(row -> {
					List<String> cellList = UploadUtils.getStream(row, colCount)
							.map((cell) -> formatter.formatCellValue(cell, evaluator))
							.collect(Collectors.toList());
					System.out.println(cellList);
					return UploadUtils.cellIteratorSupplier(colCount).get()
							.collect(Collectors.toMap(headerCells::get, cellList::get));
		}).collect(Collectors.toList());
		System.out.println(content);
		workbook.close();
		return content;
	}
}
