package com.managesmart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileToJsonFormatterApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileToJsonFormatterApplication.class, args);
	}

}
