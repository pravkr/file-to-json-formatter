package com.managesmart.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.managesmart.service.ExcelToJsonFormatter;

@RestController
@RequestMapping("/api")
public class JsonFormatterController {

	@Autowired
	Environment environment;
	
	@Autowired
	private ExcelToJsonFormatter excelToJsonFormatter;
	
	@GetMapping(value = "/format/excel", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> formatExcelToJson(@RequestParam("path") String path) {
		try {
			Path filePath = Paths.get(path);
			List<Map<String, String>> formattedExcel = excelToJsonFormatter.getJsonFormatOf(Files.newInputStream(filePath));
			return ResponseEntity.ok(formattedExcel);
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.EXPECTATION_FAILED)
					.body("{\"error\": \"Malformed path\"}");
		}
	}
	
	@GetMapping(value = "/properties", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> formatPropertiesToJson() {
		try {
			Path filePath = Paths.get(environment.getProperty("PROPERTIES_FILE_LOCATION"));
			String str = new String(Files.readAllBytes(filePath));
			String json = String.format("{%s}", str.replaceAll("\\s*([^:]+)\\s*?:\\s*?('*)([^']+)('*)(,|$)", "\"$1\": \"$3\","));
			return ResponseEntity.ok(json);
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.EXPECTATION_FAILED)
					.body("{\"error\": \"Malformed properties File\"}");
		}
	}
	
	@PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			Path filePath = Files.createFile(Paths.get(environment.getProperty("FILE_STORE_DIR"), file.getOriginalFilename() + "_" + Long.toString(Instant.now().getEpochSecond())));
	        file.transferTo(filePath);
			return ResponseEntity.ok("{\"action\": \"SUCCESS\", \"description\": \"File uploaded successfuly\", \"path\": \"" + filePath.toString() + "\"}");
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("{\"error\": \"Upload failed\"}");
		}
	}
}
